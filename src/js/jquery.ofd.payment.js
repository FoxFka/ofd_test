(function ($) {

    const methods = {
        init: function (options) {
            options = $.extend({
                url: '/json.json'
            }, options);
            var self = this;
            $.get(options.url, function (answer) {
                self.payment('update', {build: true, data: answer});
            });
            $(this).on('submit', 'form', function (e) {
                e.preventDefault();
                var data = self.payment('collectData');
                if (data) {
                    self.payment('sendData', data);
                } else {
                    self.payment('showMessage', 'Ошибка!', 'Пожалуйста, заполните все поля!');
                }
                return false;
            });
            return this;
        },
        update: function (params) {
            var self = this;
            if (params && params.hasOwnProperty('build') && params.build) {
                this.data('fields', params.data.fields);
                this.empty().append(this.payment('_buildForm', params.data));
                this.payment('changeActivePaymentType');
                this.find('.js_date-input').datepicker({
                    showAnim: "slideDown",
                    minDate: 0
                });
                this.find('.js_card-number-input').mask('9999 9999 9999 9999');
                this.on('input', '.js_amount-input', function () {
                    var re = /^\d+\.?(\d{1,2})?$/ig,
                        cVal = $.trim($(this).val());
                    $(this).val(cVal.replace(/,/g, '.'));
                    if (!re.test(cVal)) {
                        $(this).val(cVal.substr(0, cVal.length - 1));
                    }
                });
                this.on('blur', '.js_amount-input', function () {
                    var cVal = $.trim($(this).val());
                    if (/\.$/.test(cVal)) {
                        $(this).val(cVal.substr(0, cVal.length - 1));
                    }
                });
                this.on('paste', '.js_amount-input', function (e) {
                    e.preventDefault();
                });
                this.on('click', 'input[type="radio"]', function () {
                    self.data('activeType', $(this).val());
                    self.payment('changeActivePaymentType');
                });
            }
            return this;
        },
        _buildForm: function (data) {
            var result = '';
            var self = this;
            if (this.payment('objectSize', data)) {
                result = '<form class="form-block" data-provider-id="' + data.prvId + '"> \
                      <div class="text-block">' + this.payment('_buildHeader', ((data.prvName) ? data.prvName : ''), 'h3') + this.payment('_buildHeader', ((data.prvDesc) ? data.prvDesc : ''), 'h4') + '</div>';
                var fields = data.fields.sort(function (a, b) {
                    return a.orderNum - b.orderNum;
                });
                if (fields.length) {
                    fields.forEach(function (item) {
                        result += self.payment('_buildFormRow', item);
                    });
                }
                result += '<div class="form-block__row form-block__row_text-align_center"> \
                                <div class="btn-group"> \
                                    <button class="btn btn-primary btn-lg" type="submit">Отправить</button> \
                                </div> \
                            </div> \
                        </form>';
            }
            return result;
        },
        _buildHeader: function (content, type) {
            return '<div class="text-block__' + type + '">' + content + '</div>';
        },
        _buildFormRow: function (obj) {
            var res = '';
            switch (obj.fieldType) {
                case ('tSelectRadio') :
                    res = this.payment('_buildRadioRow', obj);
                    break;
                case ('tText') :
                case ('tAmount') :
                    res = this.payment('_buildTextRow', obj);
                    break;
                default :
                    break;
            }
            return res;
        },
        _buildRadioRow: function (obj) {
            var self = this;
            var res = '<div class="form-block__row"> <div class="form-block__label">' + obj.fieldLabel + '</div>';
            var items = obj.preValues.sort(function (a, b) {
                return a.orderNum - b.orderNum
            });
            res += '<div class="radio-section" data-payment-item="' + obj.fieldId + '" data-value="' + items[0].value + '">';
            items.forEach(function (item) {
                var isActive = item.orderNum === 1;
                if (isActive) {
                    self.data('activeType', item.value);
                }
                res += '<div class="radio-section__item"> \
                            <div class="radio-section__input"> \
                                <input' + (isActive ? ' checked' : '') + ' type="radio" value="' + item.value + '" id="' + obj.fieldId + '_' + item.orderNum + '" name="' + obj.fieldId + '"> \
                            </div> \
                            <label class="radio-section__label" for="' + obj.fieldId + '_' + item.orderNum + '">' + item.caption + '</label> \
                        </div>';
            });
            res += '</div></div>';
            return res;
        },
        _buildTextRow: function (obj) {
            var res = '';
            switch (obj.fieldId) {
                case ('cardNumber') :
                    var cardNumberParams = {
                        containerClassName: ' form-block__col',
                        inputClassName: ' js_card-number-input',
                        obj: obj
                    };
                    var cardDateParams = {
                        containerClassName: ' form-block__col',
                        inputClassName: ' js_date-input',
                        obj: this.data('fields').filter(function (item) {
                            return item.fieldId === 'cardDate'
                        })[0]
                    };
                    res = '<div class="form-block__row form-block__row_display_flex js_pament-type-row" data-type="cardNum">' + this.payment('_buildInput', cardNumberParams) + this.payment('_buildInput', cardDateParams) + '</div>';
                    break;
                case ('accountNumber') :
                    res = '<div class="form-block__row form-block__row_hidden js_pament-type-row" data-type="accNum">' + this.payment('_buildInput', {obj: obj}) + '</div>';
                    break;
                case ('amount') :
                    res = '<div class="form-block__row">' + this.payment('_buildInput', {
                        inputClassName: ' js_amount-input',
                        obj: obj
                    }) + '</div>';
                    break;
                default :
                    break;
            }
            return res;
        },
        _buildInput: function (params) {
            params = $.extend({
                containerClassName: '',
                inputClassName: '',
                obj: {}
            }, params);
            return '<div class="for-input' + params.containerClassName + '"> \
                        <label for="' + params.obj.fieldId + '" class="for-input__label">' + params.obj.fieldLabel + '</label>\
                        <input data-payment-item="' + params.obj.fieldId + '" type="text" name="' + params.obj.fieldId + '" id="' + params.obj.fieldId + '" class="form-control' + params.inputClassName + '" aria-label="' + params.obj.fieldLabel + '"> \
                    </div>';
        },
        changeActivePaymentType: function () {
            var type = this.data('activeType');
            this.find('.js_pament-type-row').hide();
            this.find('.js_pament-type-row[data-type="' + type + '"]').show();
        },
        collectData: function () {
            var self = this;
            var fields = this.find('[data-payment-item]');
            var data = {};
            var error = true;
            fields.each(function (index, item) {
                var value = ((item.nodeName === 'INPUT') ? $(this).val() : $(this).data('value'));
                if (error) {
                    error = self.payment('validateValue', value, $(this).data('paymentItem'));
                }
                if (value.length) {
                    data[$(this).data('paymentItem')] = value;
                }
            });
            data['prvId'] = this.find('form').data('providerId');
            return (!error) ? false : data;
        },
        objectSize: function (obj) {
            return (typeof obj === 'object') ? Object.keys.length : false;
        },
        validateValue: function (value, type) {
            var result = true;
            var activeType = $(this).data('activeType');
            switch (type) {
                case ('cardNumber') :
                    if (activeType === 'accNum') {
                        result = true;
                    } else {
                        result = value.replace(/\s/g, '').length === 16;
                    }
                    break;
                case ('cardDate') :
                    if (activeType === 'accNum') {
                        result = true;
                    } else {
                        result = value.length > 0;
                    }
                    break;
                case ('accountNumber') :
                    if (activeType === 'cardNum') {
                        result = true;
                    } else {
                        result = value.length > 0;
                    }
                    break;
                default :
                    result = value.length > 0;
                    break;
            }
            return result;
        },
        showMessage: function (title, text) {
            var content = '<div class="text-block">';
            if (title && title.length) {
                content += '<div class="text-block__h3">' + title + '</div>';
            }
            if (text && text.length) {
                content += '<div class="text-block__simple-text">' + text + '</div>';
            }
            $('.js_modal-content').empty().append(content);
            $('.js_modal').modal('show');
        },
        sendData: function (data) {
            var url = '/handler.json';
            var self = this;
            $.ajax(url, {
                data: data,
                success: function (answer) {
                    if (answer && answer.hasOwnProperty('result')) {
                        if (answer.result === 'ok') {
                            self.payment('showMessage', 'Отлично!!!', 'Все прошло хорошо. Дальше можно работать в штатном режиме.');
                        } else if (answer.result === 'fail') {
                            self.payment('showMessage', 'Ошибка!', ((answer.hasOwnProperty('reason')) ? answer.reason : 'Что то пошло не так. Пожалуйста, попробуй повторить попытку позже'));
                        }
                    }
                    self.payment('clearForm');
                },
                error: function () {
                    self.payment('showMessage', 'Ошибка!', 'Что то пошло не так! Пожалуйста, повторите попытку позже.');
                    self.payment('clearForm');
                }
            });
        },
        clearForm: function () {
            this.find('input[type="text"]').val('');
        }
    };

    jQuery.fn.payment = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.payment');
        }
    };
})(jQuery);