$(function() {
    $('.js_date-input').datepicker({
        showAnim: "slideDown",
        minDate: 0
    });
    $('.js_payment-container').payment();
});